function counterFactory() {
    let counter = 0;
    let changes =  {
        increment: function() {
            counter++;
            return counter;
        },
        decrement: function() {
            counter--;
            return counter;
        }
    };
    return changes;
};

module.exports = counterFactory;