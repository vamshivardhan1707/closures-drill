const cacheFunction = require('../cacheFunction.cjs');

function cb(...args) {
    value = args.length;
    return value;
};

const result = cacheFunction(cb);

console.log(result(1,2,3));
console.log(result('a','b'));
console.log(result(1,2,3));