const limitFunctionCallCount = require('../limitFunctionCallCount.cjs');

function cb(...args) {
     return `The arguments are ${args}`;
};

const result = limitFunctionCallCount(5,3);

console.log(result(1,2,3));
console.log(result(4,5));
console.log(result(1,2,3));
console.log(result('a','bd'));




