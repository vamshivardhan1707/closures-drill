function cacheFunction(cb) {
    if (typeof(cb) === 'function') {
        let cache = {};

        return function(...args) {
            if (args.length === 0) {
                return cb();
            };
            let key = String(args);

            if (key in cache) {
                return cache[key];
            };
            let value = cb(...args);
            cache[key] = value;

            return value;
        };
    } else {
        throw new Error ('Not a function');
    };
};

module.exports = cacheFunction;