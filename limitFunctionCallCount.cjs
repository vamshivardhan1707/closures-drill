function limitFunctionCallCount(cb, n) {
    if (typeof(cb) !== 'function' || typeof(n) !== 'number') {
        throw new Error ('Invalid Input');
    } else {
        let count = 0;

        return function(...args) {
            if (count < n) {
                count++;
                return cb(...args);
            } else {
                return null;
            };
        };
    };
};

module.exports = limitFunctionCallCount;